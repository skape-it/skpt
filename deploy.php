<?php

namespace Deployer;

use Dotenv\Dotenv;

require 'recipe/laravel.php';
require 'recipe/rsync.php';

// Get environment variables
$dotenv = Dotenv::createImmutable(__DIR__);
if (file_exists(__DIR__ . '/.env')) {
    $dotenv->load();
}

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);
set('allow_anonymous_stats', false);

// Shared files/dirs between deploys
add('shared_files', [
    '.env',
    'public/.htaccess',
    'public/robots.txt',
]);

add('shared_dirs', [
    'storage'
]);

add('writable_dirs', [

]);

// Set custom release name based on date
set('release_name', function () {
    return date('YmdHis');
});

// Keep small amount of releases
set('keep_releases', 3);

host('dev')
    ->stage('development')
    ->hostname(env('DEV_DEPLOY_HOSTNAME'))
    ->user(env('DEV_DEPLOY_USER'))
    ->multiplexing(false)
    ->port(env('DEV_DEPLOY_PORT'))
    ->set('deploy_path', env('DEV_DEPLOY_PATH'))
    ->set('rsync_src', str_replace('C:\\', '/c/', __DIR__))
    ->set('rsync_dest', '{{release_path}}');

host('prod')
    ->stage('production')
    ->hostname(env('PROD_DEPLOY_HOSTNAME'))
    ->user(env('PROD_DEPLOY_USER'))
    ->multiplexing(false)
    ->port(env('PROD_DEPLOY_PORT'))
    ->set('deploy_path', env('PROD_DEPLOY_PATH'))
    ->set('rsync_src', str_replace('C:\\', '/c/', __DIR__))
    ->set('rsync_dest', '{{release_path}}');

// Rsync options
set('rsync', [
    'exclude'       => [
        '.git',
        '.env',
        '.idea',
        '.build',
        'deploy.php',
        'storage',
        'node_modules'
    ],
    'exclude-file'  => false,
    'include'       => [],
    'include-file'  => false,
    'filter'        => [],
    'filter-file'   => false,
    'filter-perdir' => false,
    'flags'         => 'rzz', // Recursive, with compress
    'options'       => ['delete'],
    'timeout'       => 360,
]);

// Tasks
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:shared',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'artisan:migrate',
    'deploy:symlink',
    'artisan:storage:link',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');
after('deploy', 'success');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
