<?php

namespace App\Http\Requests\Admin;

use A17\Twill\Http\Requests\Admin\Request;

class RedirectRequest extends Request
{
    public function rulesForCreate()
    {
        return [
            'alias' => 'required|unique:App\Models\Redirect,alias',
            'title' => 'required',
            'url'   => 'required|url',
        ];
    }

    public function rulesForUpdate()
    {
        return $this->rulesForCreate();
    }
}
