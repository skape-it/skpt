<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\RedirectView;

class RedirectViewRepository extends ModuleRepository
{
    public function __construct(RedirectView $model)
    {
        $this->model = $model;
    }
}
