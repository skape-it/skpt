<?php

namespace App\Models;


use A17\Twill\Models\Model;
use App\User;
use Illuminate\Support\Carbon;

class Redirect extends Model
{
    protected $fillable = [
        'published',
        'title',
        'description',
        'url',
        'alias',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function views()
    {
        return $this->hasMany(RedirectView::class);
    }

    public function viewsPerDay()
    {
        $startDate = $this->created_at->format('Y-m-d');
        $endDate   = date('Y-m-d');
        $results   = [];

        $all = $this->views()->select('id', 'created_at')
                    ->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('Y-m-d'); // grouping by years
                    })
                    ->sortBy('created_at')
                    ->toArray();

        // Sort by key (date)
        ksort($all);

        if ( ! array_key_exists($startDate, $all)) {
            $results[] = array('t' => $startDate, 'y' => 0);
        }

        foreach ($all as $key => $item) {
            $results[] = array('t' => $key, 'y' => count($item));
        }

        if ( ! array_key_exists($endDate, $all)) {
            $results[] = array('t' => $endDate, 'y' => 0);
        }

        return json_encode($results);
    }
}
