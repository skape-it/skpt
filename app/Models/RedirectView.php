<?php

namespace App\Models;


use A17\Twill\Models\Model;

class RedirectView extends Model
{
    protected $fillable = [
        'view',
    ];

    public function redirect() {
        return $this->belongsTo(Redirect::class);
    }
}
