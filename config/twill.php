<?php

return [
    'enabled'      => [
        'activitylog' => true,
    ],
    'dashboard' => [
        'modules' => [
            'redirects' => [
                'name'           => 'redirects',
                'label'          => 'redirects',
                'label_singular' => 'redirect',
                'count'          => true,
                'create'         => true,
                'activity'       => true,
                'search'         => true,
            ],
        ],
    ],
];
